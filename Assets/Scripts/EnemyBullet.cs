﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

	float speed;
	Vector2 directi;
	bool isRdy;

	public AudioSource audioShoot;

	void Awake(){
		speed = 5f;
		isRdy = false;
	}

	public void SetDirection(Vector2 direction){
		directi = direction.normalized;
		isRdy = true;
		audioShoot.Play();
	}

	void Start () {
		
	}

	void Update () {
		if (isRdy) {
			Vector2 position = transform.position;
			position += directi * speed * Time.deltaTime;

			transform.position = position;		
		}
	}
	private void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Finish") {
			Destroy (gameObject);
		}
	}
}
