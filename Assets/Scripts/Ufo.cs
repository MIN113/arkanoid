﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ufo : MonoBehaviour {

	public GameObject graphics;
	public ParticleSystem explosion;

	public AudioSource audiop;

	private BoxCollider2D myCollider;

	public static SpaceManager instance;
	public static SpaceManager getInstance(){
		return instance;
	}

	void Awake () {
		myCollider = GetComponent <BoxCollider2D>();
	}

	void Update () {
		transform.Translate(Vector3.down * Time.deltaTime);
	}

	private void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet" || other.tag == "Player") {
			Explode ();
		}
		else if (other.tag == "Finish") {
			Reset ();
		}
	}
	private void Explode(){
		myCollider.enabled = false;
		SpaceManager.instance.AddHighScore(100);
		graphics.SetActive(false);
		explosion.Play();
		audiop.Play();
		Destroy (explosion, 1);
		Destroy (gameObject, 3);
	}

	protected void Reset()
	{
		myCollider.enabled = true;
		graphics.SetActive(true);
		Destroy (gameObject, 3);
	}
}
