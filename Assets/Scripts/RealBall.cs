﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RealBall : MonoBehaviour {

    public float velX;
    private float moveHorizontal;
    private Vector3 positionActual;
    public float maxX;
    public Transform barStart;

    void Start () {
		
	}
	
	void Update () {
		moveHorizontal = Input.GetAxis("Horizontal");
        //transform.Translate(velX * moveHorizontal *Time.deltaTime, 0, 0);
        positionActual = barStart.position;
        //positionActual = transform.position;

        if(positionActual.x >= maxX)
        {
            positionActual.x = maxX;
        }

        if(positionActual.x <= -maxX)
        {
            positionActual.x = -maxX;
        }

        transform.position = positionActual;
	}
}
