﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{

    public bool launched;
    protected Vector2 moveSpeed;
    protected float rotationSpeed;
    public ParticleSystem explosion;
    public AudioSource audioExpl;

    public GameObject graphics;
    private BoxCollider2D meteorCollider;

    private Vector3 iniPos;

    void Start()
    {
        iniPos = transform.position;
        meteorCollider = GetComponent<BoxCollider2D>();
        
    }

    public void LaunchMeteor(Vector3 position, Vector2 direction, float rotation)
    {
        transform.position = position;
        launched = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        moveSpeed = direction;
    }

    // Update is called once per frame
    protected void Update()
    {
        if(launched)
        {
            transform.Translate(moveSpeed.x * Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
            transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Bullet")
        {
            Explode();
        }
        else if(other.tag == "Finish")
        {
            Reset();
        }
    }

    protected void Reset()
    {
        transform.position = iniPos;
        graphics.SetActive(true);
        meteorCollider.isTrigger = true;
        launched = false;
    }

    protected virtual void Explode()
    {
        audioExpl.Play();
        SpaceManager.instance.AddHighScore(50);
        launched = false;
        graphics.SetActive(false);
        meteorCollider.isTrigger = false;
        explosion.Play();
        Invoke("Reset", 1);
    }
}