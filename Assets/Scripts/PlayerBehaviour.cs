﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {

	public Vector2 axis;
    public float speed;
    public float max;
    public float may;
    private Vector3 positionActual;
    private float moveHorizontal;
    public GameObject graphics;
    public ParticleSystem explosion;
    public AudioSource audiop;

    private BoxCollider2D myCollider;

    void Awake () {
        myCollider = GetComponent <BoxCollider2D>();
	}
	
	void Update () {
        transform.Translate(axis * speed * Time.deltaTime);
        positionActual = transform.position;

        if(positionActual.x >= max)
        {
            positionActual.x = max;
        }

        if(positionActual.x <= -max)
        {
            positionActual.x = -max;
        }

        if(positionActual.y >= may)
        {
            positionActual.y = may;
        }

        if(positionActual.y <= -may)
        {
            positionActual.y = -may;
        }

        transform.position = positionActual;
    }

    public void setAxis(Vector2 axisNow) {

        axis = axisNow;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
		if (other.tag == "Meteor"||other.tag == "EnemyB"){
            Explode();
        }
    }
    private void Explode() {
		SpaceManager. getInstance(). decLifCont();
        myCollider.enabled = false;
		speed = 0;
        graphics.SetActive(false);
        explosion.Play();
        audiop.Play();
        Invoke("Reset", 2);
    }

    private void Reset()
    {
		transform.position = new Vector3 (0, -3);
        myCollider.enabled = true;
		speed = 7;
        graphics.SetActive(true);
    }

}
