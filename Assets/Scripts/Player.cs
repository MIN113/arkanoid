﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velX;
    private float moveHorizontal;
    private Vector3 positionActual;
    public float maxX;


    void Start()
    {

    }

    private void Update()
    {

        moveHorizontal = Input.GetAxis("Horizontal");
        transform.Translate(velX * moveHorizontal *Time.deltaTime, 0, 0);
        positionActual = transform.position;

        if(positionActual.x >= maxX)
        {
            positionActual.x = maxX;
        }

        if(positionActual.x <= -maxX)
        {
            positionActual.x = -maxX;
        }

        transform.position = positionActual;
    }
}