﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    public Vector2 speed;
    public float maxVel;
    Vector2 originalPos;


    void Start()
    {
        originalPos = new Vector2(transform.position.x, transform.position.y);
    }

    void Update()
    {
        transform.Translate(speed.x * Time.deltaTime, speed.y * Time.deltaTime, 0);
    }


    void OnTriggerEnter(Collider other)
    {

        switch(other.tag)
        {

            case "Top":
                speed.y = -speed.y;
                break;
            case "Bot":
                transform.position = originalPos;
                speed.x = Random.Range (-3.0f, 3.0f);
                speed.y = -speed.y;
                break;
            case "Left":
                speed.x = -speed.x;
                break;
            case "Right":
                speed.x = -speed.x;
                break;
            case "Brick":
                speed.y = -speed.y;
                other.gameObject.GetComponent<Brick>().Touch();
                break;
            case "Player":
                speed.y = -speed.y;
                break;
        }

    }
}
