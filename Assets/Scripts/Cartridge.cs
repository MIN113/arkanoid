﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct CartridgeProperties
{
    public GameObject bulletPrefab;
    public int maxAmmo;
}


public class Cartridge
{
    private Shoot[] bullets;
    private int currentBullet = 0;

    public Cartridge(GameObject bulletPrefab, Transform parent, Vector2 origin, int maxAmmo)
    {
        bullets = new Shoot[maxAmmo];
        for(int i = 0; i < maxAmmo; i++)
        {
            //Crear la bala.
            GameObject newBullet = GameObject.Instantiate(bulletPrefab, origin, Quaternion.identity, parent);
            newBullet.name = bulletPrefab.name + "_" + i;
            //Guardar la bala en el array de balas
            bullets[i] = newBullet.GetComponent<Shoot>();

            origin.x -= 0.2f;
        }
    }

    public Shoot GetBullet()
    {
        Shoot b = bullets[currentBullet];

        currentBullet++;
        if(currentBullet >= bullets.Length) currentBullet = 0;

        return b;
    }

    ~Cartridge()
    {

    }
}