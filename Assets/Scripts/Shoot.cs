﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public float speed;
    protected bool shooting = false;
    private Vector3 iniPos;
    public AudioSource audioShoot;

    void Start()
    {
        iniPos = transform.position;
    }

    public void Shot(Vector3 position, float direction)
    {
        transform.position = position;
        shooting = true;
        transform.rotation = Quaternion.Euler(0, 0, direction);
        audioShoot.Play();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        if(shooting)
        {
            transform.Translate(0, speed * Time.deltaTime, 0);

        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Finish")
        {
            Reset();
        }
        else if (other.tag == "Meteor")
        {
            Reset();
        }
    }

    public void Reset()
    {
        transform.position = iniPos;
        shooting = false;
    }
}
