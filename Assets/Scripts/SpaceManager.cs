﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SpaceManager : MonoBehaviour {

    public static SpaceManager instance;

    private int highScore = 0;
	private int lives = 3;
    public Text highScoreText;
	public Text livesCounter;
    public GameObject pauseScreen;

	public static SpaceManager getInstance(){
		return instance;
	}

		void Awake(){
				if (instance == null) {
						instance = this;
					}
	}

    void Start () {
        instance = this;
        highScore = 0;
		lives = 3;
        highScoreText.text = highScore.ToString("D5");
		livesCounter.text = lives.ToString ();
	}

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            pauseScreen.SetActive(true);
            Time.timeScale = 0;
            
        }
    }

    public void AddHighScore(int value) {
        highScore += value;
        highScoreText.text = highScore.ToString("D5");

    }

	public void decLifCont () {
		lives--;
		livesCounter.text = livesCounter.ToString ();
	}

    public void Exit()
    {
        Debug.Log("EXIT");
        Application.Quit();
    }

    public void Resume()
    {
        Debug.Log("Resume");
        Time.timeScale = 1;
        pauseScreen.SetActive(false);
    }
}
