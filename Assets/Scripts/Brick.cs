﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    private int lifes;
    public Material red;
    public Material blue;
    public Material green;


    // Use this for initialization
    void Awake()
    {
        ChangeColor();
    }

    private void ChangeColor()
    {
        if(lifes == 3)
        {
            //color rojo
            GetComponent<Renderer>().material = red;
        }
        else if(lifes == 2)
        {
            //color azul
            GetComponent<Renderer>().material = blue;
        }
        else
        {
            //color verde
            GetComponent<Renderer>().material = green;
        }
    }

    // Update is called once per frame
    public void Touch()
    {
        lifes--;
        if(lifes <= 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            ChangeColor();
        }
    }
}